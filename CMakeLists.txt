cmake_minimum_required(VERSION 2.8)

project(audio-converter)

set(CMAKE_CXX_STANDARD 11)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")
find_package(FLAC REQUIRED)
find_package(FLAC++ REQUIRED)


include_directories("src/")

add_subdirectory("src/")

add_executable(${PROJECT_NAME} "app/main.cpp")

# Link libraries and object files
target_link_libraries(${PROJECT_NAME} decoder)
target_link_libraries(${PROJECT_NAME} FLAC)
target_link_libraries(${PROJECT_NAME} FLAC++)
