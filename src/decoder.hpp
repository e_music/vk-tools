#ifndef DECODER_HPP
#define DECODER_HPP

#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <memory>

#include <FLAC++/decoder.h>

static FLAC__uint64 total_samples = 0;
static uint32_t sample_rate = 0;
static uint32_t channels = 0;
static uint32_t bps = 0;

class OurDecoder: public FLAC::Decoder::File {
    public:
        OurDecoder(std::string fname, std::unique_ptr<std::ofstream> wav_f);
    protected:
        std::ifstream flac_file;
        std::unique_ptr<std::ofstream> wav_file;

        virtual ::FLAC__StreamDecoderWriteStatus write_callback(const ::FLAC__Frame *frame, const FLAC__int32 * const buffer[]);
        virtual void metadata_callback(const ::FLAC__StreamMetadata *metadata);
        virtual void error_callback(::FLAC__StreamDecoderErrorStatus status);
};

#endif // DECODER_HPP
