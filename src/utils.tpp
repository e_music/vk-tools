// https://stackoverflow.com/questions/105252/how-do-i-convert-between-big-endian-and-little-endian-values-in-c
namespace EMusicAudioConverter
{
    union endian_tester {
        uint32_t   n;
        uint8_t    p[4];
    };
    enum class hl_endianess {
        LittleEndian,
        BigEndian
    };

    const endian_tester sample = {0x01020304}; // this initializes .n
    hl_endianess getEndianOrder() {
        return
            (0x04 == sample.p[0])               // If Little Endian Byte Order,
                ? hl_endianess::LittleEndian
                : hl_endianess::BigEndian;
    }

    template <typename T>
    T little_endianize(T& num)
    {
        if (getEndianOrder() == hl_endianess::LittleEndian) {
            return num;
        } else {
            T littl;

            switch(sizeof(T))
            {
                case 8: //64-bit
                    ((uint8_t*)&num)[0] = ((uint8_t*)&littl)[7];
                    ((uint8_t*)&num)[1] = ((uint8_t*)&littl)[6];
                    ((uint8_t*)&num)[2] = ((uint8_t*)&littl)[5];
                    ((uint8_t*)&num)[3] = ((uint8_t*)&littl)[4];
                    ((uint8_t*)&num)[4] = ((uint8_t*)&littl)[3];
                    ((uint8_t*)&num)[5] = ((uint8_t*)&littl)[2];
                    ((uint8_t*)&num)[6] = ((uint8_t*)&littl)[1];
                    ((uint8_t*)&num)[7] = ((uint8_t*)&littl)[0];
                    break;
                case 4: //32-bit
                    ((uint8_t*)&num)[0] = ((uint8_t*)&littl)[3];
                    ((uint8_t*)&num)[1] = ((uint8_t*)&littl)[2];
                    ((uint8_t*)&num)[2] = ((uint8_t*)&littl)[1];
                    ((uint8_t*)&num)[3] = ((uint8_t*)&littl)[0];
                    break;
                case 2: //16-bit
                    ((uint8_t*)&num)[0] = ((uint8_t*)&littl)[1];
                    ((uint8_t*)&num)[1] = ((uint8_t*)&littl)[0];
                    break;
                default:
                    break;
            }

            return littl;
        }
    }

    template <typename T>
    static bool write_little_endian(std::ofstream& file, T uint_value)
    {
        try {
            file << little_endianize <T> (uint_value);
            return true;
        }
        catch (std::ofstream::failure e) {
            std::cerr << "Failed to write to file!" << std::endl;
            return false;
        }
    }
}
