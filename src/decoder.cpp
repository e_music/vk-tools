#include <iostream>
#include <fstream>

#include <unistd.h>

#include "decoder.hpp"
#include "utils.tpp"

OurDecoder::OurDecoder(std::string fname, std::ofstream wav_f) :
    FLAC::Decoder::File(),
    wav_file(std::move(wav_f))
{
    flac_file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try {
        flac_file.open(fname);
    }
    catch (std::ifstream::failure e) {
        char temp[1024];
        std::string pth = getcwd(temp, 1024)
                            ? std::string( temp )
                            : std::string("");

        std::cerr << pth << std::endl;
        std::cerr << "Error while opening file " << fname << ": "
                  << e.what() << std::endl << "Exit..." << std::endl;
        exit(1);
    }
}

::FLAC__StreamDecoderWriteStatus OurDecoder::write_callback(const ::FLAC__Frame *frame, const FLAC__int32 * const buffer[])
{
    const FLAC__uint32 total_size = static_cast<FLAC__uint32>(total_samples *
                                                              channels *
                                                              (bps / 8));
    size_t i;

    if (total_samples == 0) {
        std::cerr << "ERROR: this example only works for FLAC files "
                     "that have a total_samples count in STREAMINFO"
                  << std::endl;
        return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
    }
    if (channels != 2 || bps != 16) {
        std::cerr << "ERROR: this example only supports 16 bit "
                     "stereo streams"
                  << std::endl;
        return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
    }

    /* write WAVE header before we write the first frame */
    if (frame->header.number.sample_number == 0) {
        FLAC__uint32 chunk_size = total_size + 36;
        FLAC__uint32 subchunk1_id = 16;
        FLAC__uint16 audio_format = 1;
        FLAC__uint16 num_channels = static_cast<FLAC__uint16>(channels);
        FLAC__uint32 byte_rate = sample_rate * channels * (bps / 8);
        FLAC__uint16 block_align = channels * (bps / 8);

        *wav_file << "RIFF";
        wav_file->write(reinterpret_cast<char *>(&chunk_size), sizeof(FLAC__uint32));
        *wav_file << "WAVEfmt ";
        wav_file->write(reinterpret_cast<char *>(&subchunk1_id), sizeof(FLAC__uint32));
        wav_file->write(reinterpret_cast<char *>(&audio_format), sizeof(FLAC__uint16));
        wav_file->write(reinterpret_cast<char *>(&num_channels), sizeof(FLAC__uint16));
        wav_file->write(reinterpret_cast<char *>(&sample_rate), sizeof(FLAC__uint32));
        wav_file->write(reinterpret_cast<char *>(&byte_rate), sizeof(FLAC__uint32));
        wav_file->write(reinterpret_cast<char *>(&block_align), sizeof(FLAC__uint16)); /* block align */
        wav_file->write(reinterpret_cast<char *>(&bps), sizeof(FLAC__uint16));
        *wav_file << "data";
        wav_file->write(reinterpret_cast<char *>(const_cast<FLAC__uint32 *>(&total_size)), sizeof(FLAC__uint32));
    }

    /* write decoded PCM samples */
    for(i = 0; i < frame->header.blocksize; i++) {
        wav_file->write(reinterpret_cast<char *>(const_cast<FLAC__int32 *>(&buffer[0][i])), sizeof(FLAC__uint16));  /* left channel */
        wav_file->write(reinterpret_cast<char *>(const_cast<FLAC__int32 *>(&buffer[1][i])), sizeof(FLAC__uint16));  /* right channel */
//            std::cerr << "ERROR: write error" << std::endl;
//            return FLAC__STREAM_DECODER_WRITE_STATUS_ABORT;
    }

    return FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE;
}

void OurDecoder::metadata_callback(const ::FLAC__StreamMetadata *metadata)
{
    /* print some stats */
    if (metadata->type == FLAC__METADATA_TYPE_STREAMINFO) {
        /* save for later */
        total_samples = metadata->data.stream_info.total_samples;
        sample_rate = metadata->data.stream_info.sample_rate;
        channels = metadata->data.stream_info.channels;
        bps = metadata->data.stream_info.bits_per_sample;

        std::cerr << "sample rate:\t\t" << sample_rate << "Hz\n";
        std::cerr << "channels:\t\t" << channels << "\n";
        std::cerr << "bits per sample:\t" << bps << "\n";
        std::cerr << "total samples:\t" << total_samples << "\n";
    }
}

void OurDecoder::error_callback(::FLAC__StreamDecoderErrorStatus status)
{
    std::cerr << "Got error callback: "
              << FLAC__StreamDecoderErrorStatusString[status] << std::endl;
}
