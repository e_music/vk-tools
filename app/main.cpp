#include <iostream>
#include <memory>

#include <decoder.hpp>

int main(int argc, char *argv[])
{
    bool ok = true;


    if (argc != 3) {
        std::wcerr << "Usage: " << argv[0] << " infile.flac outfile.wav\n";
        return 1;
    }

    std::string wav_filename {argv[2]};
    std::ofstream wav_file;
    wav_file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try {
        wav_file.open(wav_filename);
    }
    catch (std::ofstream::failure e) {
        std::cerr << "Error while open " << wav_filename << " file";
        return 1;
    }

    std::string input_file {argv[1]};
    OurDecoder decoder(input_file, wav_file);

    if (!decoder) {
        fprintf(stderr, "ERROR: allocating decoder\n");
        return 1;
    }

    (void)decoder.set_md5_checking(true);

    FLAC__StreamDecoderInitStatus init_status = decoder.init(argv[1]);
    if (init_status != FLAC__STREAM_DECODER_INIT_STATUS_OK) {
        fprintf(stderr, "ERROR: initializing decoder: %s\n", FLAC__StreamDecoderInitStatusString[init_status]);
        ok = false;
    }

    if (ok) {
        ok = decoder.process_until_end_of_stream();
        fprintf(stderr, "decoding: %s\n", ok? "succeeded" : "FAILED");
        fprintf(stderr, "   state: %s\n", decoder.get_state().resolved_as_cstring(decoder));
    }

    wav_file.close();
    return 0;
}
